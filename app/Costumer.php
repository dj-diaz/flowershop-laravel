<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costumer extends Model
{
    protected $fillable = [
      'email', 'firstname', 'middlename', 'lastname', 'extname',
      'street_number', 'street_name', 'barangay', 'city', 'postal_code',
      'province',
    ];

    public function orders()
    {
      return $this->hasMany('App\Order');
    }

    public function user()
    {
    	return $this->hasOne('App\User');
    }
}
