<?php

namespace App\Http\Controllers;

use App\StockOrder;
use Gate;
use GoogleMaps;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Shop;
use App\User;
use App\Product;

class BranchController extends Controller
{
		public function index()
		{
		if(Gate::denies('shop_admin')) {
            abort(403, 'Unauthorized access!');
        }

            $branches = Shop::latest()->get();
            $disabled_branches = Shop::onlyTrashed()->get();
        return view('products.branch.index', compact('branches', 'disabled_branches'));
		}

    public function create()
    {
    	if(Gate::denies('shop_admin')) {
            abort(403, 'Unauthorized access!');
        }
        $users = User::has('shop', '<', 1)->where('role', 'branch')->get();

    	return view('products.branch.create', compact('users'));
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'city' => 'required',
            'province' => 'required',
        ]);

    		$shop = Shop::create($req->all());
        $shop->province = $req->province;
        $shop->city = $req->city;
        $shop->barangay = $req->barangay;
        $shop->user_id = $req->user_id;

       $response = \GoogleMaps::load('placeadd')
                ->setParam([
                   'location' => [
                        'lat'  => (float)$req->lat,
                        'lng'  => (float)$req->lng
                      ],
                   'accuracy'           => 0,
                   "name"               =>  $req->name,
                   "types"              => ["florist"],
                          ])
                  ->get();
        $shop->place_id = json_decode($response)->place_id;
        $shop->save();

    	return redirect(action('BranchController@show', $shop->id));
    }

    public function delete(Request $request, $id)
    {
        $shop = Shop::findOrFail($id);
        $shop->user->delete();
        $stock_orders = StockOrder::where('shop_id', $shop->id)->get();
        foreach($stock_orders as $so)
        {
            $so->delete();
        }
        $shop->delete();

        return back()->with('msg', 'Branch Deleted Successfully!');
    }

    public function enable(Request $request, $id)
    {
        $shop = Shop::withTrashed()->where('id', $id)->first();
        $shop->restore();
        $user = $shop->user();
        $stock_orders = StockOrder::where('shop_id', $shop->id)->get();
        foreach($stock_orders as $so)
        {
            $so->restore();
        }
        $user->restore();

        return back()->with('msg', 'Branch Enabled!');
    }

    public function show($id)
    {
    	$shop = Shop::withTrashed()->where('id',$id)->first();
        $products = $shop->products()->where('status','received')->paginate(7);

    	return view('products.branch.show', compact('shop', 'products'));
    }

    public function user_create()
    {
        return view('products.branch.users.create');

    }

    public function user_store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
        ]);

        User::create([
            'name' => $req->name,
            'email' => $req->email,
            'password' => bcrypt($req->password),
            'role' => 'branch',
        ]);

        $req->session()->flash('message', 'Branch Admin Created!');

        return redirect(action('BranchController@create'));
    }
}
