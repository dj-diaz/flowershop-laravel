<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;
use App\Order;
use App\User;

class BranchDashboardController extends Controller
{
    public function index(Request $req)
    {
        $shop = $req->user()->shop;
        if($shop == null) {
            abort('404', 'No Branch for this user');
        }
        $orders = $shop->orders()->where('delivered', 0)->latest()->paginate(15);
        $undelivered = $shop->orders()->where('delivered', 3)->latest()->paginate(15);
        $delivered = $shop->orders()->where('delivered', 1)->latest()->paginate(15);
        return view('shop.dashboard.index', compact('shop', 'orders', 'undelivered', 'delivered'));
    }

    public function show($code)
    {
        $order = Order::where('order_code', $code)->first();
        if(is_null($order)) {
            abort(403);
        }

        return view('shop.dashboard.show', compact('order'));
    }

    public function product_list(Request $request)
    {
        $shop = $request->user()->shop;
        $products = $shop->products();

        return view('shop.products.list', compact('products'));
    }

    public function delivered(Request $req)
    {
        $order = Order::where('order_code', $req->order_code)->first();

        if(is_null($order)) {
            abort(404);
        }

        $shop = $req->user()->shop;
        if($shop == null) {
            abort('404', 'No Branch for this user');
        }

        $shop->delivery_boy = 1;
        $shop->save();

        $order->delivered = 1;
        $order->save();

        foreach($order->products as $prod ) {
            $shop = Shop::find($order->shop_id);
            $pid = $prod->pivot->product_id;
            $product = $shop->products()->where('id', $pid)->first();
            $new_stocks = $product->pivot->stocks - $prod->pivot->quantity;
            $shop->products()->sync([$pid => ['stocks' => $new_stocks]], false);
        }

        return redirect('/branch');
    }

    public function undelivered(Request $request)
    {
        $order = Order::where('order_code', $request->order_code)->first();

        if(is_null($order)) {
            abort(404);
        }

        $shop = $request->user()->shop;
        if($shop == null) {
            abort('404', 'No Branch for this user');
        }

        $shop->delivery_boy = 1;
        $shop->save();

        $order->delivered = 3;
        $order->save();

        return redirect('/branch');
    }
}
