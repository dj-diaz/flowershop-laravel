<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Cart;
use Mail;
use GoogleMaps;
use LRedis;
use App\Http\Requests;
use App\Http\Requests\CostumerRequest;
use App\Http\Controllers\Controller;
use App\Scripts\StripeCheckout;
use App\Order;
use App\Product;
use App\Shop;

class CheckoutController extends Controller
{
    public function checkout($code)
    {
        $match = false;
        $order = Order::where('order_code' , $code)->first();
        if(is_null($order)) {
            abort(404);
        }
        $order->street_number = $order->costumer->street_number;
        $order->street_name = $order->costumer->street_name;
        $order->barangay = $order->costumer->barangay;
        $order->city = $order->costumer->city;
        $order->postal_code = $order->costumer->postal_code;
        $order->province = $order->costumer->province;
        $order->save();

        $origin = $order->barangay . ' ' . $order->city . ' ' . $order->province;
        $nearest = null;

        $geocode = GoogleMaps::load('geocoding')
            ->setParam([
                'address' => $origin,
            ])
            ->get();
        $geocode = json_decode($geocode);
        if(empty($geocode->results)) {
            $order->delete();
            alert()->error('Invalid Address');
            return redirect('/shopping-cart');
        }
        $geocode = $geocode->results[0]->geometry->location;

        $distance_array = [];
        foreach(Shop::all() as $shop) {
            $distance = $this->haversine($geocode->lat, $geocode->lng, $shop->lat, $shop->lng);
            $distance_array[$shop->id] = $distance;
        }

        asort($distance_array, SORT_NUMERIC);
        foreach($distance_array as $id => $distance)
        {
            if($distance > 9000)
            {
                $order->delete();
                alert()->error('No shop available');
                return redirect('/shopping-cart');
            }
            $shop = Shop::findOrFail($id);
            if($this->check_stocks($order, $shop->products()))
            {
                $nearest = $shop;
                if($nearest->delivery_boy == 1)
                {
                    $order->shop_id = $nearest->id;
                    $order->save();
                    $nearest->delivery_boy = 0;
                    $nearest->update();
                    return view('shop.costumers.checkout', compact('order', 'nearest'));
                }

            }
        }

        $order->delete();
        alert()->error('No shop available');
        return redirect('/shopping-cart');
    }

    public function charge(Request $request)
    {
        $order = Order::where('order_code', $request->order_code)->first();
        if(is_null($order)) {
            abort(403, 'Unauthorized Access');
        }
        StripeCheckout::charge($request->stripeToken, $order);
        $order->is_paid = 1;
        $order->save();
        return redirect('/thanks/' . $order->order_code);
    }


    public function thanks($code)
    {
        $title = 'Receipt: ' . $code;
        $order = Order::where('order_code', $code)->first();
        $shop = Shop::find($order->shop_id);
        $costumer = $order->costumer;
        Mail::send('shop.receipt.index', ['order' => $order, 'shop' => $shop], function($m) use ($title, $costumer) {
            $m->from('lafleurflowers@funcyprogrammer.com', $title);

            $m->to($costumer->email, $costumer->firstname . ' ' . $costumer->lastname)->subject($title);
        });
        $order->is_paid = 1;
        $order->save();
        Cart::destroy();
        return response()->view('shop.costumers.thanks');
    }

    public function location($code)
    {
        $order = Order::where('order_code', $code)->first();
        return view('checkout.delivery', compact('order'));
    }

    public function nearestLocation(Request $request)
    {
        $this->validate($request, [
            'street_number' => 'required|integer',
            'street_name' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'postal_code' => 'required|digits:4',
            'province' => 'required',
        ]);

        $order = Order::where('order_code', $request->order_code)->first();
        $order->street_number = $request->street_number;
        $order->street_name = $request->street_name;
        $order->barangay = $request->barangay;
        $order->city = $request->city;
        $order->postal_code = $request->postal_code;
        $order->province = $request->province;
        $order->save();

        $origin = $order->barangay . ' ' . $order->city . ' ' . $order->province;
        $nearest = null;

        $geocode = GoogleMaps::load('geocoding')
            ->setParam([
                'address' => $origin,
            ])
            ->get();
        $geocode = json_decode($geocode);
        if(empty($geocode->results)) {
            $order->delete();
            alert()->error('Invalid Address');
            return redirect('/shopping-cart');
        }
        $geocode = $geocode->results[0]->geometry->location;

        $distance_array = [];
        foreach(Shop::all() as $shop) {
            $distance = $this->haversine($geocode->lat, $geocode->lng, $shop->lat, $shop->lng);
            $distance_array[$shop->id] = $distance;
        }

        asort($distance_array, SORT_NUMERIC);
        foreach($distance_array as $id => $distance)
        {
            if($distance > 9000)
            {
                $order->delete();
                alert()->error('No shop available');
                return redirect('/shopping-cart');
            }
            $shop = Shop::findOrFail($id);
            if($this->check_stocks($order, $shop->products()))
            {
                $nearest = $shop;
                if($nearest->delivery_boy == 1)
                {
                    $order->shop_id = $nearest->id;
                    $order->save();
                    $nearest->delivery_boy = 0;
                    $nearest->update();
                    return view('shop.costumers.checkout', compact('order', 'nearest'));
                }

            }
        }
            $order->delete();
            alert()->error('No shop available');
            return redirect('/shopping-cart');
    }

    private function check_stocks($order, $shop_products)
    {
        if(is_null($order)) {
            return false;
        }
        $return_val = null;
        foreach($order->products as $prod) {
            $s_prod = $shop_products->find($prod->id);
            if(is_null($s_prod)) {
                return false;
            }
            if($prod->pivot->quantity > $s_prod->pivot->stocks) {
                return false;
            } else {
                $return_val = true;
            }
        }

        return $return_val;
    }

    private function haversine($latFrom, $lngFrom, $latTo, $lngTo)
    {
        $earthRad = 6371000;
        $latitudeF = deg2rad($latFrom);
        $longF = deg2rad($lngFrom);
        $latitudeTo = deg2rad($latTo);
        $longTo = deg2rad($lngTo);

        $latDelta = $latitudeTo - $latitudeF;
        $lonDelta = $longTo - $longF;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latitudeF) * cos($latitudeTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRad;
    }

    public function cancel_order(Request $request)
    {
        $order = Order::where('order_code', $request->order_code)->first();
        Cart::destroy();
        $shop = Shop::findOrFail($order->shop_id);
        $shop->delivery_boy = 1;
        $shop->save();
        $order->delete();
        return redirect('/');
    }
}
