<?php

namespace App\Http\Controllers;

use Alert;
use Cart;
use App\Costumer;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CostumerRequest;
use Carbon\Carbon;

class CostumerController extends Controller
{
    public function create(Request $req)
    {
        if($req->user()->costumer()->first() != null) {
            $this->costumer = $req->user()->costumer()->first();
            $orderProducts = Cart::contents();
            $order = new \App\Order;

            $order->order_code = mt_rand(100,9999) . Str::random(5);
            $order->total_purchase = Cart::total(false) + 50;
            $order->costumer_id = $this->costumer->id;
            $order->save();

            foreach($orderProducts as $prod) {
                $product = \App\Product::findOrFail($prod->id);

                $order->products()->attach($product->id, [
                    'products_total' => $order->total_purchase,
                    'price' => $prod->price,
                    'quantity' => $prod->quantity,
                ]);

            }
            return redirect("/checkout/location/" . $order->order_code);
        }
        else {
           return view('shop.costumers.create');
        }

    }

    public function store(CostumerRequest $request)
    {

        //create costumer
        $costumer = Costumer::create($request->all());
        $costumer->user_id = $request->user()->id;
        $costumer->save();
        alert()->success('Your account has been created!, Please continue', 'Success!');


        //create order associated to costumer above
        $orderProducts = Cart::contents();
        $order = new \App\Order;

        //generate random order code
        //there is a unique validator in the migration.
        //so there will not be two same order code
        $order->order_code = mt_rand(100,9999) . Str::random(5);
        $order->total_purchase = Cart::total(false) + 50;
        $order->costumer_id = $costumer->id;
        $order->save();

        foreach($orderProducts as $prod) {
          $product = \App\Product::findOrFail($prod->id);

          $order->products()->attach($product->id, [
            'products_total' => $order->total_purchase,
            'price' => $prod->price,
            'quantity' => $prod->quantity,
            ]);

        }
        return redirect("/checkout" . "/" . $order->order_code);
    }

    public function edit(Request $request)
    {   $costumer = $request->user()->costumer;
        if(is_null($costumer)) {
            return redirect('/new-costumer');
        }
        return view('shop.costumers.edit', compact('costumer'));
    }

    public function update(CostumerRequest $request)
    {
        $costumer = Costumer::where('email', $request->email)->first()->update($request->all());
        alert()->success('Your profile data is updated!', 'Success');
        return redirect('/costumer/edit');
    }
}
