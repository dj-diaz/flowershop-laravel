<?php

namespace App\Http\Controllers;
use Gate;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function authen()
    {
        if(Gate::check('costumer')){
            return redirect('/');
        } elseif (Gate::check('branch-admin')) {
            return redirect('/branch');
        } elseif(Gate::check('shop_admin')) {
            return redirect('/products');
        }
    }
}
