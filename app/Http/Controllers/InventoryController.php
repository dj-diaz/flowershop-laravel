<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

use App\StockOrder;
use App\Shop;
use App\Product;
use App\Order;
use Mail;

class InventoryController extends Controller
{
	public function index()
	{
		$stock_order = StockOrder::where('status', 'pending')->paginate(15);
		$processed_order = StockOrder::where('status', '!=', 'pending')->paginate(15);
		return view('inventory.index', compact('stock_order', 'processed_order'));
	}

	public function send_stock(Request $req)
	{
		$shop = Shop::findOrFail($req->shop_id);
		$product = Product::findOrFail($req->product_id);

		$order = StockOrder::findOrFail($req->order_id);
		$order->status = 'sent';
		$order->save();
	
		$req->session()->flash('message', 'Stocks sent, Branch admin must confirm if products are sent successfully');
		return redirect('/orders');
	}

	public function transactions()
	{
		$orders = Order::where('delivered', 1)->latest()->paginate(15);
		$total = $orders->sum('total_purchase');
		$total = number_format($total, 2);
		$quantity = 0;
		foreach($orders as $ord)
		{
			$quantity += $ord->products()->first()->pivot->quantity;
		}
		return view('products.transactions', compact('orders', 'total', 'quantity'));
	}

	public function supplier(Request $request, $id)
	{
		$this->validate($request, [
			'quantity' => 'required|integer|min:1|max:30',
		]);
		$quantity = $request->quantity;
		$product = Product::findOrFail($id);
		Mail::send('shop.products.supplier_order', ['product' => $product, 'quantity' => $quantity], function($m) use ($product) {
			$m->from('lafleurflowers@funcyprogrammer.com', "Order: " . $product->name);

			$m->to('burnie.brown@yahoo.com', 'Mr Burnie Brown')->subject('Flower Orders: ' . $product->name);
		});
		return back()->with('flash_message', 'Email sent to supplier!');
	}
	public function getLogOut(Request $reqest)
	{
		Auth::logout();
		$reqest->session()->flush();
		return redirect('/');
	}
}
