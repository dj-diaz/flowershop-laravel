<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Gate;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Requests;
use App\Http\Requests\CreateProductRequest;

use App\Shop;
use App\Product;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('shop_admin')) {
            abort(403, 'Unauthorized access!');
        }
        $products = Product::latest()->get();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buttonName = "Add Product";
        $product = new Product;
        return view('products.create', compact('buttonName', 'product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    { 
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'stocks' => $request->stocks,
            'size' => $request->size,
            'category' => $request->category,
        ]);

        $shops = Shop::all();
        foreach($shops as $shop) {
            $shop->products()->attach($product);
        }


        if($request->hasFile('image')) {
            $imageName = $product->id . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/catalog/', $imageName
            );
            $imagePath = '/images/catalog/' . $imageName;

            $product['image_path'] = $imagePath;
            $product->save();            
        }

        $request->session()->flash('flash_message', 'Product successfully added!');
        
        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buttonName = "Update Product";
        $product = Product::find($id);
        return view('products.edit', compact('buttonName', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProductRequest $request, $id)
    {
        Product::findOrFail($id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'stocks' => $request->stocks,
            'size' => $request->size,
            'category' => $request->category,
        ]);

        $product = Product::findOrFail($id);

        if($request->hasFile('image')) {
            $imageName = $product->id . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/catalog/', $imageName
            );
            $imagePath = '/images/catalog/' . $imageName;

            $product['image_path'] = $imagePath;
            $product->save();            
        }

        $request->session()->flash('flash_message', 'Product successfully updated!');

        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $product = Product::findOrFail($id);
        foreach($product->orders() as $ord)
        {
            $ord->delete();
        }
        $product->delete();
        return redirect('products');
    }

    public function logout()
    {
        return redirect('auth/logout');
    }
}
