<?php

namespace App\Http\Controllers;

use Cart;
use Auth;
use Alert;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Shop;

class ShopController extends Controller
{
    public function index()
    {
    	$products = Product::paginate(15);
    	return view('shop.gallery', compact('products'));
    }

    public function show($id)
    {
        if (Auth::guest()) {
            return redirect('/login');
        }

    	$product = Product::findOrFail($id);
    	return view('shop.show', compact('product'));
    }

    public function addToCart(Request $request, $id)
    {
        if (Auth::guest()) {
            return redirect('/login');
        }

        $this->validate($request, [
            'quantity' => 'required|integer',
        ]);

        if($request->quantity < 1)
        {
            alert()->error('Quantity must be positive value');
            return back()->with('error', 'Quantity must be greater than 1');
        }
        $product = Product::findOrFail($request->product_id);
        if($product->size == "large" && ($request->quantity > 1)) {
            return back()
                ->with('error', 'Maximum of 1 order per large flower');
        } elseif ($product->size == "medium" && $request->quantity > 2) {
            return back()
                ->with('error', 'Maximum of 2 order per medium flower');
        } elseif ($product->size == "small" && $request->quantity > 5) {
            return back()
                ->with('error', 'Maximum of 5 order per small flower');
        }
        if(count(Cart::contents(true)) == 1) {
            return back()
                ->with('error', 'Cart already full. ');
        }
        $product = Product::findOrFail($id);

        Cart::insert([
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => $request->quantity,
            'image_path' => $product->image_path,
        ]);

        alert()->success('Success!', 'Product added to cart.');
        return redirect('/');
    }

    public function cart(Request $request)
    {
        return view('shop.cart', compact('invoice'));
    }

    public function removeProductCart($item)
    {
        foreach(Cart::contents() as $product)
        {
            if ($product->name == $item)
            {
                $product->remove();
            }
        }
        return view('shop.cart');
    }

    public function updateQuantity($item, Request $request)
    {
        $product = Product::where('name', $request->name)->first();
        if(is_null($product)) abort(404);
        if($product->size == "large" && ($request->quantity > 1)) {
            return back()
                ->with('error', 'Maximum of 1 order per large flower');
        } elseif ($product->size == "medium" && $request->quantity > 2) {
            return back()
                ->with('error', 'Maximum of 2 order per medium flower');
        } elseif ($product->size == "small" && $request->quantity > 5) {
            return back()
                ->with('error', 'Maximum of 5 order per small flower');
        }
        foreach(Cart::contents() as $prod)
        {
            if($prod->name == $item)
            {
                $prod->quantity = $request->quantity;
                alert()->success('Quantity updated!', "Success!");
            }
        }

        return redirect('/shopping-cart');
    }

    public function branch_list()
    {
        $shops = Shop::orderBy('name')->get();

        return view('shop.branches', compact('shops'));
    }

    public function about()
    {
        return view('shop.about');
    }

    public function faq()
    {
        return view('shop.faq');
    }
}
