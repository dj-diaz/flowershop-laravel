<?php

namespace App\Http\Controllers;

use Auth;
use Gate;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Response;

use App\Product;
use App\Shop;
use App\StockOrder;

class ShopProductsController extends Controller
{
	public function index()
	{
		if(Gate::denies('branch-admin')) {
			abort(403, 'Unauthorized access');
		}
		$user = Auth::user();
		$shop = $user->shop;
		$products = $shop->products()->where('status','received')->paginate(15);
		return view('shop.products.index', compact('shop', 'user', 'products'));
	}

	public function restock()
	{
		$shop = Auth::user()->shop;
		$stock_order = StockOrder::where('status', "sent")->where('shop_id', $shop->id)->get();
		$received_order = StockOrder::where('status', 'received')->where('shop_id', $shop->id)->get();

		return view('shop.dashboard.restock', compact('shop', 'stock_order', 'received_order'));
	}

	public function restock_store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|exists:products',
			'amount' => 'required|integer|min:1|max:20',
			'id' => 'required|exists:shops',
		]);

		$product = Product::where('name' , $request->name)->first();
		if(is_null($product)) {
			abort('404');
		}

		$stock_order = StockOrder::create([
			'product_id' => $product->id,
			'amount' => $request->amount,
			'shop_id' => $request->id,
 		]);
		alert()->success('Restock order sent to main branch', 'Success');
		return redirect('/branch/products/restock');
	}

	public function restock_accept(Request $req)
	{
		// change status of stock order to received
		$stock_order = StockOrder::findOrFail($req->order_id);
		$stock_order->status = 'received';
		$stock_order->save();

		$shop = Shop::findOrFail($req->shop_id);
		//create a product_shop row

		if($shop->products()->where('id', $req->product_id)->exists()) {
			$product = $shop->products()->find($req->product_id);
			$new_val = $product->pivot->stocks + $stock_order->amount;
			$shop->products()->updateExistingPivot($req->product_id, ['stocks' => $new_val, 'status' => 'received'], false);
            $product->stocks -= $stock_order->amount;
            $product->save();
		} else {
			$product = Product::findOrFail($req->product_id);
			$shop->products()->attach($product->id, ['stocks' => $stock_order->amount, 'status' => 'received']);
            $product->stocks -= $stock_order->amount;
            $product->save();
		}

		alert()->success('Restock order received', 'Success');
		return redirect('/branch/products/restock');
	}

	public function getdata(Request $request)
	{

		$term = Str::lower($request->term);
	    $data = Product::latest()->get();
	    $return_array = array();

	    foreach ($data as $product) {
	        if (strpos(Str::lower($product->name), $term) !== false) {
	            $return_array[] = array('value' => $product->name);
	        }
	    }
	    return response()->json($return_array);
	}
}
