<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;

class SortController extends Controller
{
    public function large()
    {
        $products = Product::where('size', 'large')->paginate(15);
        return view('shop.gallery', compact('products'));
    }

    public function medium()
    {
        $products = Product::where('size', 'medium')->paginate(15);
        return view('shop.gallery', compact('products'));
    }

    public function small()
    {
        $products = Product::where('size', 'small')->paginate(15);
        return view('shop.gallery', compact('products'));
    }

    public function birthday()
    {
        $products = Product::where('category', 'birthday')->paginate(15);
        return view('shop.gallery', compact('products'));
    }

    public function funeral()
    {
        $products = Product::where('category', 'funeral')->paginate(15);
        return view('shop.gallery', compact('products'));
    }

    public function special()
    {
        $products = Product::where('category', 'special')->paginate(15);
        return view('shop.gallery', compact('products'));
    }

    public function daily()
    {
        $products = Product::where('category', 'daily')->paginate(15);
        return view('shop.gallery', compact('products'));
    }
}
