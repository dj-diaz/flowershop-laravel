<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CostumerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //PROBABLY implement additional authorization, if user accounts are implemented
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'email' => 'required|email',
          'firstname' => 'required|min:2|',
          'lastname' => 'required|min:2|',
          'middlename' => 'required|min:2|',
          'street_number' => 'required',
          'street_name' => 'required',
          'barangay' => 'required',
          'city' => 'required',
          'postal_code' => 'required|digits:4',
          'province' => 'required',
        ];
    }
}
