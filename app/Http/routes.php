<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'web'], function () {

	Route::get('/', ['as' => 'shop.home', 'uses' => 'ShopController@index']);
	Route::get('/about', 'ShopController@about');
	Route::get('/faq', 'ShopController@faq');
	Route::post('shop/{id}/add',[ 'as' => 'shopping-cart.add' ,'uses' => 'ShopController@addToCart']);
	Route::resource('shop', 'ShopController');
	Route::get('shopping-cart',[ 'as' => 'shopping-cart', 'uses' => 'ShopController@cart']);
	Route::post('shopping-cart/r/{item}', 'ShopController@updateQuantity');
	Route::get('shopping-cart/remove/{item}', 'ShopController@removeProductCart');

	Route::get('/checkout/location/{code}', 'CheckoutController@location');
	Route::post('/checkout/location/', 'CheckoutController@nearestLocation');
	Route::get('checkout/{code}', 'CheckoutController@checkout');
	Route::post('checkout', 'CheckoutController@charge');
	Route::get('thanks/{code}', 'CheckoutController@thanks');
	Route::post('/cancelorder', 'CheckoutController@cancel_order');

	Route::get('new-costumer', 'CostumerController@create');
	Route::post('new-costumer', 'CostumerController@store');
	Route::get('/costumer/edit', 'CostumerController@edit');
	Route::patch('/costumer/edit', 'CostumerController@update');
	//Routes for adding braches by the super admin

	Route::get('/products/branches', 'BranchController@index');
	Route::get('/products/branch/add', 'BranchController@create');
	Route::post('/products/branch/add', 'BranchController@store');
	Route::get('/products/branch/user', 'BranchController@user_create');
	Route::post('/products/branch/user', 'BranchController@user_store');
	Route::get('/products/branch/{id}', 'BranchController@show');
	Route::delete('/products/branch/{id}', 'BranchController@delete');
	Route::post('/products/branch/{id}', 'BranchController@enable');


	//Routes for branches
	Route::get('/branches_list', 'ShopController@branch_list');

	Route::auth();
	Route::get('/logout', 'InventoryController@getLogOut');

	Route::resource('products', 'ProductsController');
	Route::get('/home', 'HomeController@index');
	Route::get('/authen', 'HomeController@authen');
});
