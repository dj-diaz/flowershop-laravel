<?php

Route::group(['middleware' => 'web', 'prefix' => '/orders'], function() {
	Route::get('/', 'InventoryController@index');
	Route::post('/', 'InventoryController@send_stock');
	Route::post('/supplier/{id}', 'InventoryController@supplier');
});

Route::group(['middleware' => 'web', 'prefix' => '/transactions'], function() {
	Route::get('/', 'InventoryController@transactions');
});



