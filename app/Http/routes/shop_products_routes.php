<?php

use Illuminate\Support\Str;

Route::group(['middleware' => 'web', 'prefix' => '/branch/products'], function() {
	Route::get('/', 'ShopProductsController@index');
	Route::get('/restock', 'ShopProductsController@restock');
	Route::post('/restock', 'ShopProductsController@restock_store');
	Route::post('/restock/accept', 'ShopProductsController@restock_accept');
	Route::get('/getdata', 'ShopProductsController@getdata');
});

Route::group(['middleware' => 'web'], function() {
	Route::get('/branches', 'BranchDashboardController@index');
	Route::get('/branch', 'BranchDashboardController@index');
	Route::post('/branch', 'BranchDashboardController@delivered');
	Route::post('/branch/undelivered', 'BranchDashboardController@undelivered');
	Route::get('/branch/{code}', 'BranchDashboardController@show');
});