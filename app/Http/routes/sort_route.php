<?php

Route::group(['middleware' => 'web'], function() {
    Route::get('/large', 'SortController@large');
    Route::get('/medium', 'SortController@medium');
    Route::get('/small', 'SortController@small');
    Route::get('/birthday', 'SortController@birthday');
    Route::get('/funeral', 'SortController@funeral');
    Route::get('/special', 'SortController@special');
    Route::get('/daily', 'SortController@daily');
});
