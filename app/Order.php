<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'shop_id',
    ];
    protected $casts = [
        'is_paid' => 'boolean',
        'delivered' => 'boolean',
    ];

    public function costumer()
    {
      return $this->belongsTo('App\Costumer');
    }

    public function products()
    {
      return $this->belongsToMany('App\Product')
        ->withPivot('products_total', 'quantity', 'price')
        ->withTimestamps();
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }
}
