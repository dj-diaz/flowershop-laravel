<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $attributes = [
        'image_path' => '/images/photo-na.jpg',
    ];

    protected $fillable = [
    	'name',
    	'price',
    	'stocks',
    	'description',
        'size',
        'category',
    ];

    public function orders()
    {
    	return $this->belongsToMany('App\Product');
    }

    public function shops()
    {
        return $this->belongsToMany('App\Shop')
            ->withPivot('stocks', 'status');
    }
}
