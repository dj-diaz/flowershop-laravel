<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;
    protected $fillable = [
    	'name',
    	'lat',
    	'lng',
        'delivery_boy',
    ];

    public function orders()
    {
    	return $this->hasMany('App\Order');
    }

    public function user()
    {
	return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product')
            ->withPivot('stocks', 'status');
    }
}
