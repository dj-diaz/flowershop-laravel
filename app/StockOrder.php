<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockOrder extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'product_id', 'shop_id', 'amount',
    ];

    public function shop()
    {
    	return $this->belongsTo('App\Shop');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }
}
