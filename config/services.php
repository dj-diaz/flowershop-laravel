<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => "sandbox7ca65692dd3b4e9ca8ce97ee4483903d.mailgun.org",
        'secret' => 'key-350d53d20c08699c35fe6b2eee15a5ac',
    ],

    'mandrill' => [
        'secret' => 'tYJ1gAVPNI375Pw2vkGGxw',
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('pk_test_kHU0OmCaAnl7C2CG4CgWIGMfapp'),
        'secret' => env('sk_test_g3i2uY6twhGL7PNCkv6k79NT'),
    ],

];
