<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function(Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name');
            $table->integer('user_id')->unsigned()->default(1);
            $table->double('lat');
            $table->double('lng');
            $table->integer('street_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('barangay')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('place_id', 255)->nullable();
            $table->integer('delivery_boy')->default(1);
            $table->timestamps();
        });

        Schema::table('shops', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
    }
}
