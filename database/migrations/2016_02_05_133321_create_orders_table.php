<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->index();
            $table->string('order_code')->unique();
            $table->integer('costumer_id')->unsigned();
            $table->float('total_purchase');
            $table->integer('shop_id')->unsigned()->nullable();
            $table->integer('is_paid')->default(0);
            $table->integer('delivered')->default(0);
            $table->integer('street_number');
            $table->string('street_name');
            $table->string('barangay');
            $table->string('city');
            $table->integer('postal_code');
            $table->string('province');
            $table->timestamps();
        });

        Schema::table('orders', function(Blueprint $table) {
          $table->foreign('costumer_id')->references('id')->on('costumers');
          $table->foreign('shop_id')->references('id')->on('shops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
