<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => 'admin user',
            'email' => 'admin@example.com',
            'role' => 'admin',
            'password' => bcrypt('123123'),
        ]);

        DB::table('users')->insert([
            'name' => 'Jordan Bero',
            'email' => 'bero@example.com',
            'role' => 'branch',
            'password' => bcrypt('123123'),
        ]);

        DB::table('users')->insert([
            'name' => 'Nicole Manaloto',
            'email' => 'nmanaloto@example.com',
            'role' => 'branch',
            'password' => bcrypt('123123'),
        ]);

    // DB::table('products')->insert([
    //     'name' => 'CCCCCC',
    //     'description' => "Lorem Ipsum Dolor Ismet",
    //     'price' => 99.99,
    //     'stocks' => 50,
    // ]);

    }
}
