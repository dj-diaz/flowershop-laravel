process.env.DISABLE_NOTIFIER = true
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 require('laravel-elixir-stylus');
var nib = require('nib');
elixir(function(mix) {
    mix.stylus('app.styl');
    mix.stylus('admin.styl');
    mix.stylus('gallery.styl');
});
