function initMap() {
    var map = new google.maps.Map(document.getElementById('mapDiv'), {
        center: {
            lat: 15,
            lng: 121
        },
        zoom: 15
    });

    var geocoder = new google.maps.Geocoder();

    var address = $('#hiddenAddress').val();
    console.log(address);
    geocoder.geocode({
        'address': address
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });

}
