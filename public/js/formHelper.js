$(document).ready(function () {
	$(#deleteButton).on('click', function(e) {
		e.preventDefault();
		if(confirm('are you sure you want to delete?')) {
			$(#deleteButton).closest('form').submit();
		}
	});
});