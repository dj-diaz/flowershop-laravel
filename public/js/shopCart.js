
$(function() {
    $('#continueShopping').click(function(e) {
        e.preventDefault();
        window.location.href = "/";
    });

    var cartTotalItems = parseInt($('#cartTotalItems').text());
    if(cartTotalItems < 1) {
        $('#checkoutCart').addClass('disabled');
    }
});