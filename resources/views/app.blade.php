<html>
<head>
	<meta charset="UTF-8">
	<title>Flowershop</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/admin.css">
	<script src="/js/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	@yield('scripts')
</head>
<body>
	@include('navbar')

	@yield('content')

	@yield('footer')
</body>
</html>