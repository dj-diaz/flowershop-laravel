@extends('layouts.app')

@section('scripts')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAnfyRXcRZVRmGgwDvIulwIBWTYK3bewKk&libraries=places">
</script>
@endsection

@section('content')

<div class="col-md-5 col-md-offset-1">
  <div class="panel panel-primary">
    <div class="panel-heading">Use existing address:</div>
    <div class="panel-body">
        <div class="container">
        <h1>Use Existing address</h1>
        <strong>{{ $order->costumer->street_number . ' ' . $order->costumer->street_name}}</strong>
        <strong>{{ $order->costumer->barangay . ' ' . $order->costumer->city}}</strong>
        <strong>{{ $order->costumer->province . ' ' . $order->costumer->postal_code}}</strong> <br>
        
        <a href="{{action('CheckoutController@checkout', $order->order_code)}}" type="button" class="btn btn-success" style="padding-right: 20px"> 
    Continue</a>
        </div>
    </div>
  </div>
</div>

<div class="col-md-5">
  <div class="panel panel-primary">
    <div class="panel-heading">Different address</div>
    @if($errors->any())
    <ul class="alert alert-danger">
        @foreach($errors->all() as $err)
            <li style="list-style-type: none">{{$err}}</li>
        @endforeach
    </ul>
    @endif
    <div class="panel-body">
      {!! Form::open(['action' => 'CheckoutController@nearestLocation']) !!}
        <h1>Different address</h1>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <input type="hidden" name="order_code" value="{{$order->order_code}}">
                </div>
                <div class="form-group">
                    {!! Form::label('street_number', 'Street Number:') !!}
                    {!! Form::number('street_number', null, ['class' => 'form-control', 'placeholder' => '123']) !!}
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    {!! Form::label('street_name', 'Street Name:') !!}
                    {!! Form::text('street_name', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    {!! Form::label('barangay', 'Barangay:') !!}
                    {!! Form::text('barangay', null, ['class' => 'form-control', 'placeholder' => 'Barangay Name']) !!}
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    {!! Form::label('city', 'City:') !!}
                    {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City/Municipality']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    {!! Form::label('postal_code', 'Postal Code:') !!}
                    {!! Form::number('postal_code', null, ['class' => 'form-control', 'placeholder' => '2010']) !!}
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    {!! Form::label('province', 'Province:') !!}
                    {!! Form::text('province', null, ['class' => 'form-control', 'placeholder' => 'Province']) !!}
                </div>
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Continue"></input>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
