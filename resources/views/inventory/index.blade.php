@extends('app')

@section('content')

<div class="container">
	@if(Session::has('message'))
		<div class="alert alert-success">
			{{ Session::get('message') }}
		</div>
	@endif
	<h1> Restock Orders</h1>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<th>Branch Name</th>
				<th>Branch Admin</th>
				<th>Products</th>
				<th>Amount</th>
				<th>In Warehouse</th>
				<th>Actions</th>
			</tr>
			@foreach($stock_order as $order)
			<tr>
				<td>{{ $order->shop->name }}</td>
				<td>{{ $order->shop->user->name}}</td>
				<td>{{ $order->product->name }}</td>
				<td id="order_amount">{{ $order->amount }}</td>
				<td id="in_stock">{{ $order->product->stocks }}</td>
				<td>
					{!! Form::open(['action' => 'InventoryController@send_stock']) !!}
						<input type="hidden" name="shop_id" value="{{ $order->shop->id}} ">
						<input type="hidden" name="product_id" value="{{ $order->product->id }}">
						<input type="hidden" name="amount" value="{{ $order->amount }}">
						<input type="hidden" name="order_id" value="{{ $order->id }}">
						@if($order->amount > $order->product->stocks )
						<input type="submit" id='sendButton' type="button" class="btn btn-warning disabled" value="Not Enough Stocks">
						@else
						<input type="submit" id='sendButton' type="button" class="btn btn-warning" value="Send Stocks">
						@endif
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach
		</table>
		{!! $stock_order->links() !!}
	</div>
	
	<h1> Processed Orders</h1>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<th>Branch Name</th>
				<th>Branch Admin</th>
				<th>Products</th>
				<th>Amount</th>
				<th>Status</th>
			</tr>
			@foreach($processed_order as $order)
			<tr>
				<td>{{ $order->shop->name }}</td>
				<td>{{ $order->shop->user->name}}</td>
				<td>{{ ucfirst($order->product->name) }}</td>
				<td>{{ $order->amount }}</td>
				<td> {{ ucfirst($order->status) }}</td>
				</td>
			</tr>
			@endforeach
		</table>
        {!! $processed_order->links()  !!}

	</div>
	

	
</div>

@endsection