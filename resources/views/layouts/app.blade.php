<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>La Fleur Magnifique Flower Shop </title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    @yield('styles')
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                        <img style="max-width:40px; margin-top: -9px; margin-right: 5px;"src="https://s3.amazonaws.com/stripe-uploads/acct_17dIdgEYsMOa3udomerchant-icon-1455457255359-flower.png" alt="logo" class="img-responsive pull-left" />  La Fleur Magnifique Flower Shop
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}" >HOME</a></li>
                    <li><a href="{{action('ShopController@branch_list')}}">BRANCH LIST</a></li>
                    <li><a href="{{action('ShopController@about')}}">ABOUT US</a></li>
                    <li><a href="{{action('ShopController@faq')}}">FAQ</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">LOGIN</a></li>
                        <li><a href="{{ url('/register') }}">REGISTER</a></li>
                    @else
                        <li><a href="{{ url('/shopping-cart')}}"><span class="glyphicon glyphicon-shopping-cart"></span>Shopping Cart <span class="badge" id="cartTotalItems">{{ Cart::totalItems() }}</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{url('/costumer/edit')}}" style="color: #000;"><i class="glyphicon glyphicon-user"></i>  Edit Info</a></li>
                                <li><a href="{{ url('/logout') }}" style="color: #000;"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>

        </div>
    </nav>

    @yield('messages')

    @yield('content')

    @yield('footer')
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/js/sweetalert.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @include('sweet::alert')
    @yield('extra-scripts')

</body>
</html>
