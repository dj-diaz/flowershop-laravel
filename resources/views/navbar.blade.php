
<nav class="navbar-inverse navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#siteNavToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			<a href="{{ action('ProductsController@index') }}" class="navbar-brand">Administrator Dashboard</a>
			</div>


			<div class="collapse navbar-collapse" id="siteNavToggle">
				<ul class="nav navbar-nav">
					<li><a href="{{ action('ProductsController@index') }}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li><a href="{{ url('/products/branches')}}">Branches</a></li>
					<li><a href="{{ action('InventoryController@index') }}">Restocking Orders</a></li>
					<li><a href="{{action('InventoryController@transactions')}}">Transactions</a></li>
				</ul>
			
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ action('BranchController@create')}}">Add Branch</a></li>
					<li><a href="{{ action('ProductsController@create') }}"> <span class="glyhpicon glyphicon-plus fa-lg"></span>  Add Product</a></li>
					<li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
				</ul>
			</div>
			
		</div>
</nav>