@extends('app')

@section('scripts')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAnfyRXcRZVRmGgwDvIulwIBWTYK3bewKk&libraries=places">
</script>

@endsection

@section('content')
	<div class="container">

		@if(Session::has('message'))
			<div class="alert alert-success">{{ Session::get('message') }}</div>
		@endif

		@if($errors->any())
			<div class="alert alert-danger">
				@foreach($errors->all() as $err)
					<p>{{ $err }}</p>
				@endforeach
			</div>
		@endif
		<div class="col-sm-6">
			<h1>Add New Branch</h1>
			{{ Form::open(['action' => 'BranchController@store', 'files' => true]) }}
				<div class="form-group">
					<label for="">Branch Name:</label>
					<input type="text" class="form-control input-sm" name="name">
				</div>

				<div class="form-group">
					<label for="street_number">Street:</label>
					<input type="text" class="form-control input-sm" name="barangay" id="barangay">
					<input type="hidden" name="formatted_address" id="formatted_address">
				</div>

				<div class="form-group">
					<label for="barangay">Barangay:</label>
					<input type="text" class="form-control input-sm" name="barangay" id="barangay">
					<input type="hidden" name="formatted_address" id="formatted_address">
				</div>


				<div class="form-group">
					<label for="city">Municipality/City:</label>
					<input type="text" class="form-control input-sm" name="city" id="city">
				</div>


				<div class="form-group">
					<label for="province">Province:</label>
					<input type="text" class="form-control input-sm" name="province" id="province">
				</div>


				<div class="form-group">
					<label for="">Map</label>
					<input type="text" id="searchmap" class='form-control'>
					<div id="mapCanvas" style="width: 500px; height: 300px;"></div>
				</div>

				<div class="form-group">
					<label for="">Latitude</label>
					<input type="text" class="form-control input-sm" name="lat" id="lat">
				</div>

				<div class="form-group">
					<label for="">Longitude</label>
					<input type="text" class="form-control input-sm" name="lng" id="lng">
				</div>

				<div class="form-group">
					<label for="user_id">Select administrator</label>
					<select name="user_id" class="form-control">
						@foreach($users as $user)
							<option value="{{$user->id}}"> {{ $user->name }} </option>
						@endforeach
					</select> <br>
					<label for="create_btn">Or</label>
					<a href="{{ url('/products/branch/user')}}" data-rel="popup" name="create_btn" type="button" class="btn btn-default">Create One</a>
				</div>

				<button class="btn btn-sm btn-danger">Save</button>

			{{ Form::close() }}
		</div>

	</div>
	<script>

		var map = new google.maps.Map(document.getElementById('mapCanvas'), {
			center: {
				lat: 14.89,
				lng: 121
			},
			zoom: 12
		});

		var marker = new google.maps.Marker({
			position: {
				lat: 14.89,
				lng: 121
			},
			map: map,
			draggable: true
		});

		var searchbox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

		var geocoder = new google.maps.Geocoder;

		google.maps.event.addListener(searchbox, 'places_changed', function() {
			var places = searchbox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, place;

			for(i=0; place=places[i]; i++) {
				bounds.extend(place.geometry.location);
				marker.setPosition(place.geometry.location);
			}

			map.fitBounds(bounds);
			map.setZoom(15);
		});

		google.maps.event.addListener(marker, 'position_changed', function() {
			var lat = marker.getPosition().lat();
			var lng = marker.getPosition().lng();

			$('#lat').val(lat);
			$('#lng').val(lng);

			geocoder.geocode({'location': {lat: lat, lng: lng}}, function(results, status) {
				if(status === google.maps.GeocoderStatus.OK) {

					$('#formatted_address').val(results[1].formatted_address);
					var component = results[1].address_components;
					$.each(component, function(key, value) {


						if(value.types[0] == "administrative_area_level_2") {
							$('#province').val(value.long_name);
						} else if(value.types[0] == "locality") {
							$('#city').val(value.long_name);
						} else if(value.types[0] == "neighborhood") {
							$('#barangay').val(value.long_name);
						}
					});

				}
			});
		});
	</script>
@endsection
