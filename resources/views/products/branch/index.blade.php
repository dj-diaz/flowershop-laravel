@extends('app')

@section('content')
	<div class="container">
		@if(Session::has('msg'))
			<div class="alert alert-success">
				<p>{{Session::get('msg')}}</p>
			</div>
		@endif
		<div class="row">
		<h1>Branches List</h1>
		<hr>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>Branch Name</th>
					<th></th>
					<th>More Info</th>
					<th>Delete Branch</th>
				</tr>
				@foreach($branches as $branch)
					<tr>
						<td>{{ $branch->name }}</td>
						<td></td>
						<td><a href="{{action('BranchController@show', $branch->id)}}" type="button" class="btn btn-info"> <span class="glyphicon glyphicon-info-sign"></span> Info</a></td>
						<td>
							{!! Form::open(['method' => 'DELETE', 'action' => ['BranchController@delete', $branch->id]]) !!}
							<button type="submit" class="btn btn-danger" id="deleteButton"><span class="glyphicon glyphicon-trash"></span>   Disable</button>
							{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
			</table>
		</div>
		</div>

		<div class="row">
			<h1>Disabled Branches</h1>

			<div class="table-responsive">
				<table class="table">
					<tr>
						<th>Branch Name</th>
						<th></th>
						<th>More Info</th>
						<th>Delete Branch</th>
					</tr>

					@foreach($disabled_branches as $branch)
						<tr>
							<td>{{ $branch->name }}</td>
							<td></td>
							<td><a href="{{action('BranchController@show', $branch->id)}}" type="button" class="btn btn-info"> <span class="glyphicon glyphicon-info-sign"></span> Info</a></td>
							<td>
								{!! Form::open(['action' => ['BranchController@enable', $branch->id]]) !!}
								<button type="submit" class="btn btn-success" id="deleteButton"><span class="glyphicon "></span>   Enable</button>
								{!! Form::close() !!}
							</td>
						</tr>
					@endforeach

				</table>
			</div>
		</div>
	</div>
@endsection