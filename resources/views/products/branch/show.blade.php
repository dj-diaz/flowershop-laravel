@extends('app')

@section('scripts')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAnfyRXcRZVRmGgwDvIulwIBWTYK3bewKk&libraries=places">
</script>

@endsection

@section('content')
	<div class="container">
		<h1>{{ $shop->name }}</h1>
		<h4><strong>Branch administrator: </strong> {{ $shop->user->name }}</h4>
		<hr>

		<div id="showMapCanvas" class="col-md-6"></div>
		<div class="col-md-6">
			<h3>Products</h3>
			<div class="table-responsive">
				<table class="table">
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>Stocks</th>
					</tr>

					@foreach($products as $prod)
					<tr>
						<td>{{ $prod->name }}</td>
						<td> ₱{{ $prod->price }}</td>
						<td>{{ $prod->pivot->stocks }}</td>
					</tr>
					@endforeach
				</table>
				{!! $products->links()  !!}
			</div>
		</div>
	</div>

	<script>

		var lat = {{ $shop->lat }};
		var lng = {{ $shop->lng }};
		var map = new google.maps.Map(document.getElementById('showMapCanvas'), {
			center: {
				lat: lat,
				lng: lng
			},
			zoom: 15
		});

		var marker = new google.maps.Marker({
			position: {
				lat: lat,
				lng: lng
			},
			map: map
		});

		var info = new google.maps.InfoWindow({
			content: "{{ $shop->name }}",
		});

		info.open(map, marker);

		google.maps.event.addListener(marker, 'click', function() {
			info.open(map, marker);
		});
	</script>
@endsection
