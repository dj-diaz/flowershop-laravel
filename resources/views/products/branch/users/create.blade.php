@extends('app')

@section('content')
<div class="container">
	<div class="well">
		@if($errors->any())
		<ul class="alert alert-danger">
			@foreach($errors->all() as $err)
				<li style="list-style-type: none">{{ $err }}</li>
			@endforeach
		</ul>
		@endif
		
			<h3>Create Branch Administrator</h3>
			<hr>
			{!! Form::open(['action' => 'BranchController@user_store']) !!}
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control">
				</div>

				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" class="form-control">
				</div>

				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control">
				</div>

				<button type="submit" class="btn btn-danger"> Save</button>
			{!! Form::close() !!}
			
	</div>
</div>

@endsection
	