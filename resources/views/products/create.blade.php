@extends('app')

@section('content')
	<div class="container">
		<h1>Add New Product</h1>

		<hr>
		
		@if($errors->any())
			<ul class="alert alert-danger">
				@foreach($errors->all() as $error)
					<li style="list-style-type: none">{{$error}}</li>
				@endforeach				
			</ul>

		@endif
			
		
		{!! Form::open(['action' => 'ProductsController@store', 'files' => 'true']) !!}
			@include('products.form')
		{!! Form::close() !!}

	</div>
@stop