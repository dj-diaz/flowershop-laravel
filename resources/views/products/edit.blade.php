@extends('app')

@section('content')
	<div class="container">
		@if($errors->any())
			<ul class="alert alert-danger">
				@foreach($errors->all() as $err)
					<li style="list-style-type: none">{{$err}}</li>
				@endforeach
			</ul>
		@endif
		<h1>Edit: {{ $product->name }}</h1>
		<hr>
		{!! Form::model($product, ['method' => 'PATCH','files' => true, 'action' => ['ProductsController@update', $product->id]]) !!}
		@include('products.form')
		{!! Form::close() !!}
	</div>

@stop