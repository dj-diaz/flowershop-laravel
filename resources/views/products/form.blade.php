<div class="col-md-7">
			<div class="form-group">
				{!! Form::label('name', 'Product name:') !!}
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
			</div>
			
			<label for="price">Product price:</label>
			<div class="form-group input-group">
				<span class="input-group-addon">₱</span>
				<input type="number" step="any" min="1" id="price" name="price" value="{{ $product->price }}" class="form-control"> 
			</div>
	
			<div class="form-group">
				{!! Form::label('description', 'Product description:') !!}
				{!! Form::textArea('description', null, ['class' => 'form-control']) !!}
			</div>

			<div class="form-group">
				<label for="stocks">Stocks: (In Warehouse)</label>
				<input type="number" class="form-control" name="stocks" value="{{ $product->stocks }}">
			</div>

			<div class="form-group">
				<label for="size">Size:</label>
				<select name="size" id="size" class="form-control">
					<option value="large">Large</option>
					<option value="medium">Medium</option>
					<option value="small">Small</option>
				</select>
			</div>

			<div class="form-group">
				<label for="size">Category:</label>
				<select name="category" id="category" class="form-control">
					<option value="birthday">Birthday</option>
					<option value="funeral">Funeral</option>
					<option value="special">Special</option>
					<option value="daily">Daily Bouquet</option>
				</select>
			</div>

			<div class="form-group">
				{!! Form::submit($buttonName, ['class' => 'form-control btn btn-primary'] ) !!}
			</div>
		</div>

		<div class="col-md-5">
			
			<div class="row">
				<div class="form-group">
					{!! Form::label('image', 'Product Image:') !!}
					{!! Form::file('image', null) !!}
				</div>
			</div>
		</div>