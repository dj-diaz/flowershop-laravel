@extends('app')

@section('content')
<div class="container">
	@if(Session::has('flash_message'))
		<div class="alert alert-success">
			{{ Session::get('flash_message') }}
		</div>
	@endif

		@if($errors->any())
			<ul class="alert alert-danger">
				@foreach($errors->all() as $err)
					<li style="list-style-type: none">{{$err}}</li>
				@endforeach
			</ul>
		@endif
		<h1>Products in warehouse</h1>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>Product Name</th>
					<th>Price</th>
					<th>Stocks</th>
					<th>More details</th>
					<th>Edit</th>
					<th>Order from Supplier</th>
					<th>Delete</th>
				</tr>
				@foreach($products as $product)
					<tr>
						<td>{{ $product->name }}</td>
						<td>₱ {{ $product->price }}</td>
						@if($product->stocks <= 10)
							<td style="color: #880000"><b>{{ $product->stocks }}</b></td>
						@else
							<td>{{ $product->stocks }}</td>
						@endif

						<td><a href="{{ action('ProductsController@show', $product->id) }}" type="button" class="btn btn-info"><span class="glyphicon glyphicon-info-sign"></span>  Info</a></td>
						<td><a href="{{ action('ProductsController@edit', $product->id) }}" type="button" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Edit</a></td>
						<td>
							{!! Form::open(['action' => ['InventoryController@supplier', $product->id]]) !!}
							<div class="row">
								<input type="number" name="quantity" class="pull-left col-md-6">
								<button type="submit" class="btn btn-default pull-left" id="supplyButton"><span
											class="glyphicon glyphicon-send"></span> Send
								</button>
								{!! Form::close() !!}
							</div>
						</td>
						<td>
							{!! Form::open(['method' => 'DELETE', 'action' => ['ProductsController@destroy', $product->id], 'onsubmit' => 'return ConfirmDelete()']) !!}
								<button type="submit" class="btn btn-danger" id="deleteButton"><span class="glyphicon glyphicon-trash"></span>   Delete</button>
							{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
			</table>
		</div>

	</div>
@stop

@section('scripts')
	<link rel="stylesheet" href="/js/jquery.min.js">
	<link rel="stylesheet" href="/js/formHelper.js">
@stop
