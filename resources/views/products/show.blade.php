@extends('app')

@section('content')
	<div class="container">
		<h1>{{ $product->name }}</h1>

		<hr>

		<div class="col-md-7">
			<div class="row">
				{{ $product->description }}
			</div>
			
			<div class="row">
				Price: <strong>₱ {{$product->price}}</strong>
			</div>

			<div class="row">
				Stocks: <strong>{{ $product->stocks }}</strong>
			</div>			
		</div>

		<div class="col-md-5">
			<img src="{{ $product->image_path }}" alt="{{ $product->name}}" class="img-responsive">
		</div>
			
	</div>
@stop