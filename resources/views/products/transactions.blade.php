@extends('app')

@section('content')
	<div class="container">
		<h1>Transactions</h1>
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>Order #</th>
					<th>Amount</th>
					<th>Branch</th>
					<th>Product name</th>
					<th>Quantity</th>
				</tr>
				@if(! is_null($orders))
					@foreach($orders as $order)
					<tr>
						<td>{{$order->order_code}}</td>
						<td id="amount">₱ {{ $order->total_purchase  }}</td>
						<td>{{$order->shop->name}}</td>
						<td>{{$order->products()->first()->name}}</td>
						<td>{{$order->products()->first()->pivot->quantity}}</td>
					</tr>
					@endforeach
				@endif
			</table>
			<h2>Total: ₱ {{$total}}</h2>
			<h2>Total Quantity: {{$quantity}}</h2>
		</div>
		{!! $orders->links() !!}
	</div>
@endsection