@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary">
            <div class="panel-heading">About Us</div>

            <div class="panel-body">
                <h3>About us</h3>
                <p style="font-size: 18px">Lafleur Magnifique Flower Shop is a chain of flower shop across the country that provides an online delivery service.
                    Since the shop started way back 2004 it provided physical shops around Philippines and now with the newest features which is the online delivery service that guarantees a delivery within 30 minutes of your desired destination whenever a nearest physical shop is available.

                    Browse branches by visiting our branch page or start picking your desired bouquet ready for delivery on our homepage.

                    Never let your loved ones not feel your love and thought because of distance, try Lafleur Magnifique Flower Shop online delivery now and let them know how you feel about them.</p>
            </div>
        </div>
    </div>
@endsection