@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary">
            <div class="panel-heading">Branch List</div>

            <div class="panel-body">

                @foreach($shops as $shop)
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">{{$shop->name}}</div>
                        <div class="panel-body">
                            {{$shop->name}} <br>
                            {{$shop->barangay . ' ' . $shop->city . ' ' . $shop->province}}
                            <div class="table-responsive">
                                <table class="table">
                                <tr>
                                    <th>Product Name</th>
                                    <th>Stocks</th>
                                </tr>
                                @foreach($shop->products()->where('status', 'received')->get() as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->pivot->stocks}}</td>
                                </tr>
                                @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection