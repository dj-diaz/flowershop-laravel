@extends('layouts.app')

@section('styles')
    <meta name="_token" content="{{ csrf_token() }}" />
    <script src="/js/flowerCart.js"></script>
@endsection

@section('content')
    <div class="container">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">Shopping Cart:</div>

            <div class="panel panel-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(Cart::contents() as $item)
                        <tr>
                            <td class="col-sm-8 col-md-6">
                                <div class="media">
                                    <a class="thumbnail pull-left" href="#"> <img class="media-object" src="{{$item->image_path}}" style="width: 72px; height: 72px;"> </a>
                                    <div class="media-body">
                                        <h4 class="media-heading" id="itemName">{{$item->name}}</h4>
                                    </div>
                                </div></td>
                            <td class="col-sm-2 col-md-2">

                                {!! Form::open(['action' => ['ShopController@updateQuantity', $item->name], 'class' => 'form-inline', 'role' => 'form'])!!}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="name" value="{{$item->name}}">
                                <div class="col-sm-10 col-md-10">
                                    <input type="number" class="form-control" name="quantity" id="productQuantity" value="{{ $item->quantity }}">
                                </div>
                                <div class="col-sm-2 col-md-2">
                                    <button type="submit" class="btn btn-info"> <span class="glyphicon glyphicon-refresh"></span></button>
                                </div>
                                {!! Form::close() !!}
                            </td>
                            <td class="col-sm-1 col-md-1 text-center"><strong id="itemPrice">₱{{$item->price}}</strong></td>
                            <td class="col-sm-1 col-md-1 text-center"><strong id="itemTotal">₱{{$item->price * $item->quantity}}</strong></td>
                            <td class="col-sm-1 col-md-1">
                                <!-- <button type="button" class="btn btn-danger" id="removeCart">

                                </button -->
                                <a href="{{ action('ShopController@removeProductCart', $item->name) }}" type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-remove"></span> Remove </a>
                            </td>

                        </tr>
                    @endforeach

                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Subtotal</h5></td>
                        <td class="text-right"><h5><strong>₱{{ Cart::total(false) }}</strong></h5></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Estimated shipping</h5></td>
                        <td class="text-right"><h5><strong>₱50</strong></h5></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong>₱ {{ Cart::total(false) + 50 }}</strong></h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                            <button type="submit" class="btn btn-default" id="continueShopping">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                            </button></td>
                        <td>
                            <a href="{{ action('CostumerController@create') }}" type="button" class="btn btn-success" id="checkoutCart">
                                Checkout <span class="glyphicon glyphicon-play"></span>
                            </a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop


@section('extra-scripts')
    <script type="text/javascript" src="/js/shopCart.js"></script>
@endsection
