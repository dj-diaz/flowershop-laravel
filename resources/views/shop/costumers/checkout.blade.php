@extends('layouts.app')
@section('styles')


@endsection
@section('content')
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-primary">
      <div class="panel-heading">Payment: </div>
      <div class="panel-body">
        <input type="hidden" id="hiddenAddress" value="{{ $order->barangay . ' ' . $order->city . ' ' . $order->province  }}">
        <h3>Approximate Delivery Location: {{ $order->barangay . ' ' . $order->city . ' ' . $order->province }}</h3>
        <div id="mapDiv" style="width: 450px; height: 400px;">

        </div>
        <h3>Nearest Available branch: <strong>{{ $nearest->name }}</strong></h3>


        <form action="/checkout" method="POST">
          <script
                  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                  data-key="pk_test_kHU0OmCaAnl7C2CG4CgWIGMf"
                  data-image="https://s3.amazonaws.com/stripe-uploads/acct_17dIdgEYsMOa3udomerchant-icon-1455457255359-flower.png"
                  data-name="La Fleur Magnifique Flowershop"
                  data-description="Payment"
                  data-amount="{{ $order->total_purchase * 100}}"
                  data-currency="PHP"
                  data-locale="auto">
          </script>
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
          <input type="hidden" value="{{$order->order_code}}" name="order_code"/>
        </form>

        <form action="/cancelorder" method="post">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="order_code" value="{{$order->order_code}}">
          <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Cancel</button>
        </form>
      </div>
    </div>
  </div>

@endsection

@section('extra-scripts')
  <script type="text/javascript" src="/js/core.js"></script>
  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAnfyRXcRZVRmGgwDvIulwIBWTYK3bewKk&callback=initMap"></script>
@endsection
