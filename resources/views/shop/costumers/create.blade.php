@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        @if($errors->any())
          <ul class="alert alert-danger">
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
          </ul>
        @endif

        {!! Form::open(['action' => 'CostumerController@store']) !!}
            @include('shop.costumers.form')
        {!! Form::close() !!}
        <i>By pressing continue, you certify that the information above are true and correct.</i>
    </div>

    <script type="text/javascript">
    </script>
@endsection
