@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        @if($errors->any())
          <ul class="alert alert-danger">
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
          </ul>
        @endif


		{!! Form::model($costumer, ['method' => 'PATCH', 'action' => 'CostumerController@update']) !!}
			@include('shop.costumers.form');
		{!! Form::close() !!}
	</div>
@endsection