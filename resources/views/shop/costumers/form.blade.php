<div class="panel panel-primary">
    <div class="panel-heading">Enter your information: </div>
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
    @endif
    <div class="panel-body">
        <div class="form-group">
            {!! Form::label('firstname', 'First Name:') !!}
            {!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('middlename', 'Middle Name:') !!}
            {!! Form::text('middlename', null, ['class' => 'form-control', 'placeholder' => 'Middle Name']) !!}
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-7 col-lg-7">
                <div class="form-group">
                    {!! Form::label('lastname', 'Last Name:') !!}
                    {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                </div>
            </div>
            <div class="col-md-5 col-sm-5 col-lg-5">
                <div class="form-group">
                    {!! Form::label('extname', 'Suffix:') !!}
                    {!! Form::text('extname', null, ['class' => 'form-control', 'placeholder' => '(Optional) Jr. Sr. II']) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'example@gmail.com']) !!}
        </div>
    </div>
</div>


 <div class="panel panel-primary">
        <div class="panel-heading">Enter your location information:</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('street_number', 'Street Number:') !!}
                        {!! Form::number('street_number', null, ['class' => 'form-control', 'placeholder' => '123']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        {!! Form::label('street_name', 'Street Name:') !!}
                        {!! Form::text('street_name', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('barangay', 'Barangay:') !!}
                        {!! Form::text('barangay', null, ['class' => 'form-control', 'placeholder' => 'Barangay Name']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        {!! Form::label('city', 'City:') !!}
                        {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City/Municipality']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('postal_code', 'Postal Code:') !!}
                        {!! Form::number('postal_code', null, ['class' => 'form-control', 'placeholder' => '2010']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        {!! Form::label('province', 'Province:') !!}
                        {!! Form::text('province', null, ['class' => 'form-control', 'placeholder' => 'Province']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <button type="submit" class="btn btn-success btn-block btn-lg">
                    <span class="glyphicon glyphicon-play fa-lg"></span> Continue</button>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-danger btn-block btn-lg" id="cancelBtn">
                    <span class="glyphicon glyphicon-remove fa-lg"></span> Cancel</button>
                </div>
            </div>
        </div>
    </div>
