@extends('layouts.app')

@section('content')
  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-primary">
      <div class="panel-heading">Info: </div>

      <div class="panel-body">

        <div class="col-md-4 col-sm-4 col-lg-4 col-md-offset-4">
          <h1 class="text-center">Thank you!</h1>
          <h3 class="text-center">Your Order is being processed</h3>
          <h4 class="text-center">Your flowers will be delivered in 20 - 30 minutes.</h4>
          <h5 class="text-center">A copy of your reciept is sent to your email</h5>
        </div>

      </div>
    </div>
@endsection
