@extends('shop.dashboard.app')



@section('content')
	<h1>Orders: {{ $shop->name }}</h1>
	<div class="table-responsive">

		<table class="table">
			<tr>
				<th>Order Code</th>
				<th>Amount</th>
				<th>Paid</th>
				<th>Actions</th>
				<th>Details</th>
				<th></th>
			</tr>

			@foreach($orders as $ord)
			<tr>
				<td id="orderNumber">{{$ord->order_code }}</td>
				<td id="amount">₱ {{ $ord->total_purchase  }}</td>
				@if($ord->is_paid)
					<td id="paid"><i class="glyphicon glyphicon-ok"> </i></td>
				@else
					<td id="paid"> <i class="glyphicon glyphicon-remove"> </i> </td>
				@endif
				<td id="details">
					{!! Form::open() !!}
						<input type="hidden" name="order_code" value="{{ $ord->order_code }}">
						<input type="submit" type="button" class="btn btn-warning" value="Delivered">
					{!! Form::close() !!}
				</td>
				<td>
					<a href="{{action('BranchDashboardController@show', $ord->order_code)}}" type="button" class="btn btn-info">Info</a>
				</td>
				<td>
					{!! Form::open(['action' => 'BranchDashboardController@undelivered']) !!}
					<input type="hidden" name="order_code" value="{{ $ord->order_code }}">
					<input type="submit" type="button" class="btn btn-danger" value="Not Delivered">
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach

		</table>
		{!! $orders->links() !!}
	</div>

@endsection

@section('extra_content')
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Not Delivered Orders</div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Order Code</th>
                            <th>Amount</th>
                            <th>Paid</th>
                        </tr>

                        @foreach($undelivered as $u)
                            <tr>
                                <td>{{$u->order_code}}</td>
                                <td id="amount">₱ {{ $u->total_purchase  }}</td>
                                @if($u->is_paid)
                                    <td id="paid"><i class="glyphicon glyphicon-ok"> </i></td>
                                @else
                                    <td id="paid"> <i class="glyphicon glyphicon-remove"> </i> </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
                {!! $undelivered->links() !!}
            </div>
		</div>
	</div>

    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">Delivered Orders</div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Order Code</th>
                            <th>Amount</th>
                            <th>Paid</th>
                        </tr>

                        @foreach($delivered as $d)
                            <tr>
                                <td>{{$d->order_code}}</td>
                                <td id="amount">₱ {{ $d->total_purchase  }}</td>
                                @if($d->is_paid)
                                    <td id="paid"><i class="glyphicon glyphicon-ok"> </i></td>
                                @else
                                    <td id="paid"> <i class="glyphicon glyphicon-remove"> </i> </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
                {!! $delivered->links() !!}
            </div>
        </div>
    </div>
@endsection
