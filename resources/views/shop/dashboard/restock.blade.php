@extends('shop.dashboard.app')

@section('content')
<style>
body .ui-autocomplete {
  background: rgba(192, 192, 192, 1);
  border: 0px;
  border-radius: 2px;
  position: absolute;
  left: 50%;
  margin-left: -336px;
  display: none;
  outline: none;
  z-index: 1000;
  list-style-type: none;
}
body .ui-autocomplete .ui-menu-item .ui-corner-all {
  background: rgba(0, 0, 0, 0.7);
  font-family: 'Montserrat', sans-serif;
  font-weight: normal;
  font-size: 14px;
  display: block;
  clear: both;
  line-height: 20px;
  color: #fff;
  outline-color: #000;
  list-style-type: none;
}
body .ui-autocomplete .ui-menu-item .ui-state-focus {
  background: white;	
  border: 0px;
  list-style-type: none;
}
</style>
@if($errors->any())
	<ul class="alert alert-danger">
		@foreach($errors->all() as $err)
			<li style="list-style-type: none;">{{ $err }}</li>
		@endforeach
	</ul>
@endif
{!! Form::open(['action' => 'ShopProductsController@restock_store']) !!}
	<div class="forn-group">
		<label for="name">Product name:</label>
		<input type="text" name="name" id="auto" class="form-control"></select>
	</div>

	<div class="forn-group">
		<label for="amount">Amount:</label>
		<input type="number" name="amount" id="amount" class="form-control">	
	</div>

	<div class="form-group">
		<input type="hidden" name="id" value="{{ $shop->id }}">
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-success"> Request Stocks</button>		
	</div>
	
{!! Form::close() !!}

<script type="text/javascript">
	$(function() {
		$('#auto').autocomplete({
			source: "/branch/products/getdata",
			minLenth: 1,
			select: function(event, ui) {
				$('#response').val(ui.item.id);
			}
		});
	});
</script>
@endsection

@section('extra_content')
<div class="panel panel-default">
	<div class="panel-heading">Pending Requests:</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>Product Name:</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>

				@foreach($stock_order as $order)
				<tr>
					<td>{{ $order->product->name }}</td>
					<td>{{ $order->amount }}</td>
					<td>{{ ucfirst($order->status) }} </td>
					<td>
						{!! Form::open(['action' => 'ShopProductsController@restock_accept']) !!}
							<input type="hidden" name="order_id" value="{{ $order->id }}">
							<input type="hidden" name="shop_id" value="{{ $order->shop->id }}">
							<input type="hidden" name="product_id" value="{{ $order->product_id }}">
							<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Receive</button>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Received:</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table">
				<tr>
					<th>Product Name:</th>
					<th>Amount</th>
					<th>Status</th>
				</tr>

				@foreach($received_order as $order)
				<tr>
					<td>{{ $order->product->name }}</td>
					<td>{{ $order->amount }}</td>
					<td>{{ ucfirst($order->status) }} </td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection