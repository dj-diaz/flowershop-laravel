@extends('shop.dashboard.app')

@section('content')
	<h1>Order: {{$order->order_code}}</h1>
	<hr>
	<h3>Name: {{ $order->costumer->firstname . ' '. $order->costumer->middlename .' '.  $order->costumer->lastname . ' '.  $order->costumer->extname }}</h3>
	<hr>
	<h3>Delivery Location: {{ $order->street_number . ' ' . $order->street_name . ' ' . $order->barangay . ' ' . $order->city . ' ' . $order->province . ' ' . $order->postal_code}}</h3>
	<hr>
	<h3>Products: </h3>
	<hr>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<th>Product Name</th>
				<th>Quantity</th>
			</tr>
			<tr>
				@foreach($order->products as $prod)
				<td>{{ $prod->name }}</td>
				<td>{{ $prod->pivot->quantity }}</td>
				@endforeach
			</tr>
		</table>
	</div>
	
@endsection