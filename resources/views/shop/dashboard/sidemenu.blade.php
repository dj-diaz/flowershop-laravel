<div class="side-menu">
		<nav class="navbar navbar-default" role="navigation">
			<!-- Main Menu -->
			<div class="side-menu-container">
				<ul class="nav navbar-nav">
					<li class="active"><a href="{{ action('BranchDashboardController@index')}}"><span class="glyphicon glyphicon-dashboard"></span> Home</a></li>
					<li><a href="{{ action('ShopProductsController@index')}}"><span class="glyphicon glyphicon-shopping-cart"></span> Products List</a></li>
					<li><a href="{{ action('ShopProductsController@restock')}}"><span class="glyphicon glyphicon-plus-sign"></span> Request Restocks</a></li>


				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>

	</div>
