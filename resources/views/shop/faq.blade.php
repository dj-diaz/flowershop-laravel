@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Frequently Asked Questions</div>

            <div class="panel-body container">

                <p style="font-size: 18px">Frequently Asked Questions :
                    <div class="row">
                        <p class="container">
                    1. How to Order ? <br>
                    * A customer needs to create an account first before they can make purchases by clicking the “Register” button on the upper right corner of the home page.
                    <br>
                    * Next is to navigate and pick your desired products. <br>
                    * By clicking the “CART” button you will be redirected to the checkout page <br>
                    * Fill up the required details and after that you can enter your payment details <br>
                    * Once payment is confirmed we will inform you if a nearest branch is available, either the stocks or delivery boy is available.</p>
                    </div>


                   <div class="row">
                       <p class="container">
                           2. What type of payment is accepted ? <br>
                           * Our company only accepts online payment but guarantees a secured transaction <br>
                           * To avoid bogus buying we excluded the COD option. <br>

                       </p>
                   </div>

                    <div class="row">
                        <p class="container">
                            3. Who can Order ? <br>
                            * Anyone who have an access of an internet and has a browser anywhere in the world as long as the destination of your gift has a nearest physical shop to deliver your order.
                        </p>
                    </div>

                    <div class="row">
                        <p class="container">
                        4. How about if the gift was not delivered ? <br>
                        * If your order was not received by your loved ones, our management is notified and your payment will be refunded once our payment partner is notified about the unlikely situation.</p>
                        </p>
                    </div>
                </div>
            </div>
@endsection