@extends('layouts.app')

@section('styles')
	<link rel="stylesheet" href="/css/gallery.css">
@endsection

@section('content')
	<div class="col-md-2">
		<div class="panel panel-primary">
			<div class="panel-heading">Browse by categories:</div>

			<div class="panel-body">
				<ul class="list-group">
					<a href="{{action('SortController@birthday')}}" class="list-group-item">Birthday Flowers</a>
					<a href="{{action('SortController@funeral')}}" class="list-group-item">Funeral</a>
					<a href="{{action('SortController@special')}}" class="list-group-item">Special Occassions</a>
					<a href="{{action('SortController@daily')}}" class="list-group-item">Daily Bouquet</a>
				</ul>

				<ul class="list-group">
					<a href="/" class="list-group-item list-group-item-info">Show All</a>
				</ul>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">Browse by size:</div>

			<div class="panel-body">

				<ul class="list-group">
					<a href="{{url('/large')}}" class="list-group-item">Large</a>
					<a href="{{url('/medium')}}" class="list-group-item">Medium</a>
					<a href="{{url('/small')}}" class="list-group-item">Small</a>
				</ul>

				<ul class="list-group">
					<a href="/" class="list-group-item list-group-item-info">Show All</a>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div class="panel panel-primary">
			<div class="panel-heading"> Our Products: </div>

			<div class="panel-body">
				@if($errors->any())
					<ul class="alert alert-danger">
						@foreach($errors->all() as $error)
							<li style="list-style-type: none">{{ $error }}</li>
						@endforeach
					</ul>
				@endif

				@if(Session::has('flash_message'))
					<div class="alert alert-success">
						{{ Session::get('flash_message') }}
					</div>
				@endif

				@if(Session::has('error'))
					<div class="alert alert-danger">
						{{Session::get('error')}}
					</div>
				@endif
				
				<div class="row">
					@foreach($products as $product)
						@include('shop.product-tile')
					@endforeach

					{!! $products->render() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

