
<div class="col-sm-4 col-lg-4 col-md-4">

    <div class="thumbnail" id="product-tile">
        <img src="{{ $product->image_path }}" alt="{{ $product->name }}" class="" id="image-thumbnail">
        <div class="caption">
            <h4 class="pull-right">₱ {{ $product->price }}</h4>
            <h4><a href="{{ action('ShopController@show', $product->id) }}">{{substr($product->name, 0, 15)}}</a>
            </h4>
            <p>{{ substr($product->description, 0, 30)}}... <a href="{{ action('ShopController@show', $product->id) }}">More Details ...</a>.</p>
        </div>

        <div class="add-form">
            {!! Form::open(['action' => ['ShopController@addToCart', $product->id]]) !!}
                <input type="hidden" name="product_id" value="{{$product->id}}">
                {!! Form::number('quantity') !!}
                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-shopping-cart"></span>   Add to Cart</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
