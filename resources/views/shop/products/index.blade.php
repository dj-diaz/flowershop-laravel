@extends('shop.dashboard.app')


@section('content')
	<div class="table-responsive">
		@if(is_null($products))
			<h1>No Products.</h1>
		@else
			<table class="table">
				<tr>
					<th>Product Name</th>
					<th>Price</th>
					<th>Stocks</th>
				</tr>
				@foreach($products as $prod)
					<tr>
						<td> {{ $prod->name }} </td>
						<td> ₱{{ $prod->price }} </td>
						<td> {{ $prod->pivot->stocks }} </td>
					</tr>
				@endforeach
			</table>
		@endif
	</div>
@endsection
