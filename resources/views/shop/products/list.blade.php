@extends('shop.dashboard.app)

@section('content)
    @if(is_null($products))
        <h1>No Products.</h1>
    @else
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Stocks</th>
                </tr>

                @foreach($products as $product)
                   <tr>
                       <th>{{$product->name}}</th>
                       <th>{{$product->price}}</th>
                       <th>{{$product->pivot->stocks}}</th>
                   </tr>
                @endforeach
            </table>
        </div>
    @endif
@endsection