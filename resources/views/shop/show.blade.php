@extends('layouts.app')

@section('content')
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-primary">
			<div class="panel-heading"> {{ $product->name }} </div>

			<div class="panel-body">
				@if($errors->any())
					<ul class="alert alert-warning">
						@foreach($errors->all() as $err)
							<li style="list-style-type: none">{{$err}}</li>
						@endforeach
					</ul>
				@endif
				<div class="col-md-7">
					<img src="{{ $product->image_path }}" alt="{{ $product->name }}" class="img-responsive">
				</div>
				<div class="col-md-5">

					<h1> {{ $product->name }} </h1>
					<hr>
					<p> {{ $product->description }} </p>
					<h3>Price: ₱{{ $product->price }}</h3>
					<hr>
					{!! Form::open(['action' => ['ShopController@addToCart', $product->id]]) !!}
						<input type="hidden" name="product_id" value="{{$product->id}}">
						<label for="quantity">Quantity</label>
						<input type="number" name="quantity">
						<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection