@extends('layouts.app')

@section('content')
	<a href="#" id="get_location">Get Location</a>
	<div id="map">
		<iframe src="https://maps.google.com.ph?output=embed" frameborder="0" id="google_map" width="640" height="480"></iframe>
	</div>

@endsection

@section('extra-scripts')
	<script type="text/javascript" src="/js/flowerCart.js"></script>
@endsection