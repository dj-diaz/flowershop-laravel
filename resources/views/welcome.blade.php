@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">welcome! </div>

                <div class="panel-body">
                    Welcome to Tasty Flowershop!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
