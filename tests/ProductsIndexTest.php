<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductsIndexTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testLogin()
    {
    	$this->visit('/login')
    		 ->type('admin@example.com', 'email')
    		 ->type('123123', 'password')
    		 ->press('Login')
    		 ->seePageIs('/products');
    }
    // public function testProductsIndex()
    // {
    //     $this->visit('/products')
    //     	 ->see('Add Product')
    //     	 ->seePageIs('/products/create');
    // }
}
